#include <cmath>
#include <iostream>
#include <vector>

#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python.hpp>

#include <vector>
#include <string>

using namespace std;

class Particle {
    public:
        Particle(double x, double y, double z) : x(x), y(y), z(z) {}
        double x, y, z;

        double get_x() { return x; }
        void set_x(double x) { this->x = x; }
        
        double get_y() { return y; }
        double get_z() { return z; }
   
    private:
    protected:
};

class World
{
    public:

        string msg;

        World (string msg) : msg(msg) {} // constructor

        vector<Particle*> Particles;

        void set(string msg) { this->msg = msg; }

        string greet() { 
            string full_message = "Hello from " + msg + "!";
            cout << full_message << endl;
            return full_message; 
        }

        void make_particle_list(){
            for (int i = 0; i < 10; i++){
                Particles.push_back(new Particle(i, i, i));
            }
        }

        boost::python::list get_python_objects(){
            
            boost::python::list py_list;
        
            for (int i = 0; i < Particles.size(); i++){
                py_list.append(Particles[i]);
            }
            return py_list;
        }

    private:
    protected:
};

using namespace boost::python;

BOOST_PYTHON_MODULE(test)
{
    class_<World>("World", init<std::string>())
        .def("greet", &World::greet)
        .def("set", &World::set)
        .def("make_particle_list", &World::make_particle_list)
        .def("get_python_objects", &World::get_python_objects)
    ;

    class_<Particle>("Particle", no_init)
        .def("get_x", &Particle::get_x)  
        .def("set_x", &Particle::set_x)  
        .def("get_y", &Particle::get_y)  
        .def("get_z", &Particle::get_z)  
        
        .def_readwrite("x", &Particle::x)
        .def_readwrite("y", &Particle::y)
        .def_readwrite("z", &Particle::z)
    ;
}

