% system parameters, to be set by the user
length_in_x = 10000;             % system size in x (in m)
length_in_y = 10000;             % system size in y (in m)
dim = 250;                      % number of grid points (same in x and y)
dt = 1*365*86400;               % timestep (in seconds)
total_time = 10000*365*86400;   % total time (in seconds)

molten = false
% datacursormode on

% properties host rock (metasediments)
conductivity_h = 2.9;   % W/(m*K)
density_h = 2700;       % kg/m^3
heat_capacity_h = 300;  % J/(kg*K) [this one has been taken from granite]
temp_h = 100;           % °C

% properties melt (gabbro)
conductivity_m = 3.0;   % W/(m*K)
density_m = 3000;       % kg/m^3
heat_capacity_m = 590;  % J/(kg*K)
temp_m = 1200;          % °C

% calculation of step size (spatial and temporal)
nb_timesteps = total_time / dt; % the number of timesteps
dx = length_in_x / dim;         % spacing in x (in m)
dy = length_in_y / dim;         % spacing in y (in m)

% first we need to construct several (2D-)matrices, where the temperature at
% the time of intrusion and the relevant material parameters are defined
% for every point in the grid
T = ones(dim,dim)*temp_h;
T(int32(35*dim/100):int32(65*dim/100),int32(35*dim/100):int32(65*dim/100))=temp_m;
T(int32(45*dim/100):int32(55*dim/100),int32(45*dim/100):int32(55*dim/100))=temp_h;
T(int32(65*dim/100):int32(100*dim/100),int32(46*dim/100):int32(54*dim/100))=temp_m;

C = ones(dim,dim)*conductivity_h;
C(int32(35*dim/100):int32(65*dim/100),int32(35*dim/100):int32(65*dim/100))=conductivity_m;
C(int32(45*dim/100):int32(55*dim/100),int32(45*dim/100):int32(55*dim/100))=conductivity_h;
C(int32(65*dim/100):int32(100*dim/100),int32(46*dim/100):int32(54*dim/100))=conductivity_m;

D = ones(dim,dim)*density_h;
D(int32(35*dim/100):int32(65*dim/100),int32(35*dim/100):int32(65*dim/100))=density_m;
D(int32(45*dim/100):int32(55*dim/100),int32(45*dim/100):int32(55*dim/100))=density_h;
D(int32(65*dim/100):int32(100*dim/100),int32(46*dim/100))=density_m;

H = ones(dim,dim)*heat_capacity_h;
H(int32(35*dim/100):int32(65*dim/100),int32(35*dim/100):int32(65*dim/100))=heat_capacity_m;
H(int32(45*dim/100):int32(55*dim/100),int32(45*dim/100):int32(55*dim/100))=heat_capacity_h;
H(int32(65*dim/100):int32(100*dim/100),int32(46*dim/100))=heat_capacity_m;

%imagesc(T)
%imagesc(D)
%imagesc(C)
%imagesc(H)

%keyboard;

ALPHA = (C.*dt) ./ (2.*D.*H.*dx.*dx);
BETA = (C.*dt) ./ (2.*D.*H.*dy.*dy);
BUFFER = ones(dim,dim) * temp_h;

% used to store the tridiagonal coefficient matrix
a = zeros(dim,1);
b = zeros(dim,1);
c = zeros(dim,1);
d = zeros(dim,1);

% used to solve the matrices
c2 = zeros(dim,1);
d2 = zeros(dim,1);

% axes
X = linspace(0, length_in_x, 10);
Y = linspace(0, length_in_y, 10);

for timestep = 1:nb_timesteps
    
    display(timestep)
    
    % first part of the calculation (timestep n -> n+1/2)
    % x-sweep
    for i=2:dim-1
        % create the implicit coefficient matrix for the first halfstep
        a = -ALPHA(i,:);
        c = -ALPHA(i,:);
        b = 2.0.*ALPHA(i,:) + 1;
        
        b(1) = 1;
        b(dim) = 1;
        a(1) = 0;
        a(dim) = 0;
        c(1) = 0;
        c(dim) = 0;

        % calculate the explicit solution vector first halfstep
        d(1) = T(i,1);
        d(dim) = T(i,dim);
        for j=2:dim-1
            d(j) = BETA(i,j)*T(i-1,j) + (1.0 - 2.0*BETA(i,j))*T(i,j) + BETA(i,j)*T(i+1,j);
        end 

        % now solve the system with the Thomas algorithm
        c2(1) = c(1) / b(1);
        d2(1) = d(1) / b(1);

        for j=2:dim
            c2(j) = c(j) / (b(j) - c2(j-1)*a(j));	
            d2(j) = (d(j) - d2(j-1)*a(j)) / (b(j) - c2(j-1)*a(j));	
        end

        BUFFER(i,dim) = d2(dim);
        for j=dim-1:-1:1
            BUFFER(i,j) = d2(j) - c2(j)*BUFFER(i,j+1);
        end
    end

    %imagesc(BUFFER);

    % second part of the calculation (timestep n+1/2 -> n+1)
    % y-sweep
    for j=2:dim-1
        % create the implicit coefficient matrix for the first halfstep
        a = -BETA(:,j);
        c = -BETA(:,j);
        b = 2.0.*BETA(:,j) + 1;
        b(1) = 1;
        b(dim) = 1;
        a(1) = 0;
        a(dim) = 0;
        c(1) = 0;
        c(dim) = 0;

        % calculate the explicit solution vector first halfstep
        d(1) = T(1,j);
        d(dim) = T(dim,j);
        for i=2:dim-1
            d(i) = ALPHA(i,j)*BUFFER(i,j-1) + (1.0 - 2.0*ALPHA(i,j))*BUFFER(i,j) + ALPHA(i,j)*BUFFER(i,j+1);
        end

        % now solve the system with the Thomas algorithm
        c2(1) = c(1) / b(1);
        d2(1) = d(1) / b(1);

        for i=2:dim
            c2(i) = c(i) / (b(i) - c2(i-1)*a(i));
            d2(i) = (d(i) - d2(i-1)*a(i)) / (b(i) - c2(i-1)*a(i));
        end

        T(dim,j) = d2(dim);
        for i=dim-1:-1:1
            T(i,j) = d2(i) - c2(i)*T(i+1,j);
        end
    end
    
%     clf()
%     imagesc(X,Y,T)
%     colorbar();
%     title(strcat('Time: ',num2str(timestep * dt), ' s, ', num2str(timestep * dt / 86400), ' d, ', num2str(timestep * dt / 86400 / 365 ), ' a'))
%     %pause (0.1);
%     keyboard();
    
    if (T(dim/2, dim/2) >= 900 && ~molten)
        molten = true
        display ('MOLTEN')
        clf();
        imagesc(X,Y,T);
        colorbar();
        title(strcat('Melt. Time: ',num2str(timestep * dt), ' s, ', num2str(timestep * dt / 86400), ' d, ', num2str(timestep * dt / 86400 / 365 ), ' a'));
        %pause (0.1);
        keyboard();
    end

    if (T(dim/2, dim/2) <= 900 && molten)
        molten = false
        display ('SOLID')
        clf();
        imagesc(X,Y,T);
        colorbar();
        title(strcat('Solid. Time: ',num2str(timestep * dt), ' s, ', num2str(timestep * dt / 86400), ' d, ', num2str(timestep * dt / 86400 / 365 ), ' a'));
        %pause (0.1);
        keyboard();
    end
    
    %keyboard;
end

clf()
imagesc(X,Y,T)
colorbar();
title(strcat('Time: ',num2str(timestep * dt), ' s, ', num2str(timestep * dt / 86400), ' d, ', num2str(timestep * dt / 86400 / 365 ), ' a'))
%pause (0.1);
keyboard();