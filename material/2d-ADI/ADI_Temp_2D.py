#!/bin/env pyhton

#%% import
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import plotly.express as px

#%% system constants and variables

# system parameters, to be set by the user
length_in_x = 10000            # system size in x (in m)
length_in_y = 10000            # system size in y (in m)
dim = 250                      # number of grid points (same in x and y)
dt = 1*365*86400               # timestep (in seconds)
total_time = 10000*365*86400   # total time (in seconds)

print(dim)
#%% material properties

# host rock
conductivity_h = 2.9   # W/(m*K)
density_h = 2700       # kg/m^3
heat_capacity_h = 300  # J/(kg*K) [this one has been taken from granite]
temp_h = 100           # °C

# properties melt (gabbro)
conductivity_m = 3.0   # W/(m*K)
density_m = 3000       # kg/m^3
heat_capacity_m = 590  # J/(kg*K)
temp_m = 1200          # °C

# calculation of step size (spatial and temporal)
nb_timesteps = int(total_time / dt) # the number of timesteps
nb_timesteps = 3                    # for debugging only
dx = length_in_x / dim              # spacing in x (in m)
dy = length_in_y / dim              # spacing in y (in m)

# axes
X = np.linspace(0, length_in_x, dim)
Y = np.linspace(0, length_in_y, dim)

#%% construct matrices

T = np.ones((dim,dim)) * temp_h
T[int(0.35*dim):int(0.65*dim),int(0.35*dim):int(0.65*dim)] = temp_m
T[int(0.65*dim):dim,int(0.46*dim):int(0.54*dim)] = temp_m

C = np.ones((dim,dim)) * conductivity_h
C[int(0.35*dim):int(0.65*dim),int(0.35*dim):int(0.65*dim)] = conductivity_m
C[int(0.65*dim):dim,int(0.46*dim):int(0.54*dim)] = conductivity_m

D = np.ones((dim,dim))*density_h
D[int(0.35*dim):int(0.65*dim),int(0.35*dim):int(0.65*dim)] = density_m
D[int(0.65*dim):dim,int(0.46*dim):int(0.54*dim)] = density_m

H = np.ones((dim,dim))*heat_capacity_h
H[int(0.35*dim):int(0.65*dim),int(0.35*dim):int(0.65*dim)] = heat_capacity_m
H[int(0.65*dim):dim,int(0.46*dim):int(0.54*dim)] = heat_capacity_m

#fig, axs = plt.subplots(1,1)

#axs.imshow(T, cmap = 'plasma')
#axs[1].imshow(C, cmap = 'plasma')
#axs[2].imshow(D, cmap = 'plasma')
#axs[3].imshow(H, cmap = 'plasma')

#fig.tight_layout()
#%% build helper matrices
ALPHA = (C*dt) / (2*D*H*dx*dx)
BETA = (C*dt) / (2*D*H*dy*dy)
BUFFER = np.ones((dim,dim)) * temp_h

print (ALPHA)

T_hist = []
T_hist.append(T.copy())

#fig, ax = plt.subplots()
#ax.imshow(ALPHA, cmap = 'plasma')
#fig.tight_layout()

#%% main part and solve

# used to store the tridiagonal coefficient matrix
a = np.zeros(dim)
b = np.zeros(dim)
c = np.zeros(dim)
d = np.zeros(dim)

# used to solve the matrices
c2 = np.zeros(dim)
d2 = np.zeros(dim)

for timestep in range(0, nb_timesteps):
    
    print (timestep)

    # first part of the calculation (timestep n -> n+1/2)
    # x-sweep
    for i in range (1,dim-2):
        
        # create the implicit coefficient matrix for the first halfstep
        a = -ALPHA[i,:]
        b = 2.0*ALPHA[i,:] + 1
        c = -ALPHA[i,:]
        
        a[0] = 0
        a[dim-1] = 0
        b[0] = 1
        b[dim-1] = 1
        c[0] = 0
        c[dim-1] = 0

        # calculate the explicit solution vector for the first halfstep
        d[0] = T[i,0]
        d[dim-1] = T[i,dim-1]
        for j in range(1, dim-2):
            d[j] = BETA[i,j]*T[i-1,j] + (1.0 - 2.0*BETA[i,j])*T[i,j] + BETA[i,j]*T[i+1,j]

        # now solve the system with the Thomas algorithm
        c2[1] = c[1] / b[1]
        d2[1] = d[1] / b[1]

        for j in range(1, dim-1):
            c2[j] = c[j] / [b[j] - c2[j-1]*a[j]]
            d2[j] = (d[j] - d2[j-1]*a[j]) / (b[j] - c2[j-1]*a[j])

        BUFFER[i,dim-1] = d2[dim-1]
          
        for i in range (1, dim-2):
            for j in range(dim-2, 1,-1):
                BUFFER[i,j] = d2[j] - c2[j] * BUFFER[i,j+1]

    # second part of the calculation (timestep n+1/2 -> n+1)
    # y-sweep
    '''
    for j in range(1, dim-2):
        # create the implicit coefficient matrix for the first halfstep
        a = -BETA[:,j]
        c = -BETA[:,j]
        b = 2.0*BETA[:,j] + 1
        
        b[0] = 1.
        b[dim-1] = 1.
        a[0] = 0.
        a[dim-1] = 0.
        c[0] = 0.
        c[dim-1] = 0.

        # calculate the explicit solution vector first halfstep
        d[0] = T[0,j]
        d[dim-1] = T[dim-1,j]
        for i in range (1, dim-2):
            d[i] = ALPHA[i,j] * BUFFER[i,j-1] + (1.0 - 2.0 * ALPHA[i,j]) * BUFFER[i,j] + ALPHA[i,j] * BUFFER[i,j+1]

        # now solve the system with the Thomas algorithm
        c2[1] = c[1] / b[1]
        d2[1] = d[1] / b[1]

        for i in range(1, dim-2):
            c2[i] = c[i] / (b[i] - c2[i-1] * a[i])
            d2[i] = (d[i] - d2[i-1] * a[i]) / (b[i] - c2[i-1]*a[i])

        T[dim-1, j] = d2[dim-1]
        for i in range (dim-2, 0, -1):
            T[i,j] = d2[i] - c2[i]*T[i+1,j]
        '''
        
    T_hist.append(BUFFER.copy())
#%% display

def anim_3D(X, Y, L, timesteps, images, save = False, myzlim = (-0.15, 0.15)):
    
    fig = plt.figure(figsize = (8, 8))
    
    fig = px.imshow([[1, 20, 30],
                 [20, 1, 60],
                 [30, 60, 1]])
    fig.show()

    
    ax = fig.add_subplot(111, projection='3d')
    SX, SY = np.meshgrid(X,Y)
    
    surf = ax.plot_surface(SX, SY, L[0],cmap = plt.cm.RdBu_r)
    ax.set_zlim(myzlim[0], myzlim[1])
    ax.set_title("t = 0 s", fontsize = 16)
    
    # animation
    def surface(num):
        ax.clear()
        #surf = ax.plot_surface(SX, SY, L[images*num,:,:],cmap = plt.cm.viridis)
        ax.set_xlabel("x [m]", fontname = "serif", fontsize = 14)
        ax.set_ylabel("y [m]", fontname = "serif", fontsize = 14)
        ax.set_zlabel("$u$ [m]", fontname = "serif", fontsize = 16)
        ax.set_title("$u(x,y)$ à t = {} s".format(np.round(images*num*timesteps, 4)), fontsize = 16)
        ax.set_zlim(myzlim[0], myzlim[1])
        plt.tight_layout()
        return surf,
        
    # call the animator.  blit=True means only re-draw the parts that have changed.
    anim = animation.FuncAnimation(fig, surface, frames = L.shape[0]//images, interval = 50, blit = False)
    
    # Save the result
    if save:
        writer = animation.FFMpegWriter(fps = 24, bitrate = 10000, codec = "libx264", extra_args = ["-pix_fmt", "yuv420p"])
        anim.save('file.mp4',writer=writer)
    
    return anim

def anim_2D(X, Y, L, timesteps, images, save = False):
    
    anim = px.imshow(L, animation_frame = 0)
    
    # Save the result
    if save:
        writer = animation.FFMpegWriter(fps = 24, bitrate = 10000, codec = "libx264", extra_args = ["-pix_fmt", "yuv420p"])
        anim.save('file.mp4',writer=writer)
    
    return anim

#%% main
#if __name__ == "__main__":
T_hist = np.array(T_hist)
anim = anim_2D(X, Y, T_hist, dt, nb_timesteps, save=False)
anim.show()

#%% end of file