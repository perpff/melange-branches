#!/bin/env python

#%% import
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import plotly.express as px
import plotly.graph_objects as go

#%% system constants and variables

# system parameters, to be set by the user
length_in_x = 10000            # system size in x (in m)
length_in_y = 10000            # system size in y (in m)
length_in_z = 10000            # system size in z (in m)
dim = 100                       # number of grid points (same in x and y)
dt = 1*365*86400               # timestep (in seconds)
total_time = 10000*365*86400   # total time (in seconds)

print(dim)
#%% material properties

# host rock
conductivity_h = 2.9   # W/(m*K)
density_h = 2700       # kg/m^3
heat_capacity_h = 300  # J/(kg*K) [this one has been taken from granite]
temp_h = 100           # °C

# properties melt (gabbro)
conductivity_m = 3.0   # W/(m*K)
density_m = 3000       # kg/m^3
heat_capacity_m = 590  # J/(kg*K)
temp_m = 1200          # °C

# calculation of step size (spatial and temporal)
nb_timesteps = int(total_time / dt) # the number of timesteps
nb_timesteps = 50                   # for debugging only
dx = length_in_x / dim              # spacing in x (in m)
dy = length_in_y / dim              # spacing in y (in m)

X, Y, Z = np.mgrid[0:dim, 0:dim, 0:dim]

#%% construct T-matrice and diplay

T = np.ones((dim,dim,dim)) * temp_h
T[int(0.35*dim):int(0.65*dim),int(0.35*dim):int(0.65*dim),int(0.4*dim):int(0.6*dim)] = temp_m
T[int(0.65*dim):dim,int(0.46*dim):int(0.54*dim),int(0.4*dim):int(0.6*dim)] = temp_m
T[int(0.46*dim):int(0.54*dim), int(0.65*dim):dim,int(0.4*dim):int(0.6*dim)] = temp_m
T[int(0.46*dim):int(0.54*dim), 0:int(0.65*dim),int(0.4*dim):int(0.6*dim)] = temp_m
T[0:int(0.65*dim),int(0.46*dim):int(0.54*dim),int(0.4*dim):int(0.6*dim)] = temp_m

#%% construct remaining matrices
C = np.ones((dim,dim,dim)) * conductivity_h
C[int(0.35*dim):int(0.65*dim),int(0.35*dim):int(0.65*dim),int(0.4*dim):int(0.6*dim)] = conductivity_m
C[int(0.65*dim):dim,int(0.46*dim):int(0.54*dim),int(0.4*dim):int(0.6*dim)] = conductivity_m
C[int(0.46*dim):int(0.54*dim), int(0.65*dim):dim,int(0.4*dim):int(0.6*dim)] = conductivity_m
C[int(0.46*dim):int(0.54*dim), 0:int(0.65*dim),int(0.4*dim):int(0.6*dim)] = conductivity_m
C[0:int(0.65*dim),int(0.46*dim):int(0.54*dim),int(0.4*dim):int(0.6*dim)] = conductivity_m

D = np.ones((dim,dim,dim))*density_h
D[int(0.35*dim):int(0.65*dim),int(0.35*dim):int(0.65*dim),int(0.4*dim):int(0.6*dim)] = density_m
D[int(0.65*dim):dim,int(0.46*dim):int(0.54*dim),int(0.4*dim):int(0.6*dim)] = density_m
D[int(0.46*dim):int(0.54*dim), int(0.65*dim):dim,int(0.4*dim):int(0.6*dim)] = density_m
D[int(0.46*dim):int(0.54*dim), 0:int(0.65*dim),int(0.4*dim):int(0.6*dim)] = density_m
D[0:int(0.65*dim),int(0.46*dim):int(0.54*dim),int(0.4*dim):int(0.6*dim)] = density_m

H = np.ones((dim,dim,dim))*heat_capacity_h
H[int(0.35*dim):int(0.65*dim),int(0.35*dim):int(0.65*dim),int(0.4*dim):int(0.6*dim)] = heat_capacity_m
H[int(0.65*dim):dim,int(0.46*dim):int(0.54*dim),int(0.4*dim):int(0.6*dim)] = heat_capacity_m
H[int(0.46*dim):int(0.54*dim), int(0.65*dim):dim,int(0.4*dim):int(0.6*dim)] = heat_capacity_m
H[int(0.46*dim):int(0.54*dim), 0:int(0.65*dim),int(0.4*dim):int(0.6*dim)] = heat_capacity_m
H[0:int(0.65*dim),int(0.46*dim):int(0.54*dim),int(0.4*dim):int(0.6*dim)] = heat_capacity_m

#%% show and test
fig = go.Figure(
    data=go.Volume(
            x=X.flatten(),
            y=Y.flatten(),
            z=Z.flatten(),
            value=T.flatten(),
            isomin=700,
            isomax=1200,
            opacity=0.1, # needs to be small to see through all surfaces
            surface_count=40, # needs to be a large number for good volume rendering
            # slices_x=dict(show=True, locations=[int(dim/2)]),
            # slices_y=dict(show=True, locations=[int(dim/2)]),
            slices_z=dict(show=True, locations=[int(dim/2)]),
            
        ),
        #title_text="Temperature",
    )

fig.update_layout(
    dict(
        title="Temperature", 
        ),
    )

fig.show()

#%% build helper matrices
ALPHA = (C*dt) / (2*D*H*dx*dx)
BETA = (C*dt) / (2*D*H*dy*dy)
BUFFER = np.ones((dim,dim)) * temp_h

#%% main part and solve

# used to store the tridiagonal coefficient matrix
a = np.zeros(dim)
b = np.zeros(dim)
c = np.zeros(dim)
d = np.zeros(dim)

# used to solve the matrices
c2 = np.zeros(dim)
d2 = np.zeros(dim)

def ADI(base_vec, num):

    if base_vec == 0:
        T_plane = T[num, :, :]
        ALPHA_plane = ALPHA[num, :, :]
        BETA_plane = BETA[num, :, :]
    elif base_vec == 1:
        T_plane = T[:, num, :]
        ALPHA_plane = ALPHA[:, num, :]
        BETA_plane = BETA[:, num, :]
    elif base_vec == 2:
        T_plane = T[:, :, num]
        ALPHA_plane = ALPHA[:, :, num]
        BETA_plane = BETA[:, :, num]
    else:
        print ("There can be only dimensions between 0 and 2.")
        exit(0)

    # first part of the calculation (timestep n -> n+1/2)
    # x-sweep
    for i in range (1, dim-1):

        # create the implicit coefficient matrix for the first halfstep
        a = -ALPHA_plane[i,:]
        b = 2.0*ALPHA_plane[i,:] + 1
        c = -ALPHA_plane[i,:]

        a[0] = 0
        a[dim-1] = 0
        b[0] = 1
        b[dim-1] = 1
        c[0] = 0
        c[dim-1] = 0

        # calculate the explicit solution vector for the first halfstep
        d[0] = T_plane[i,0]
        for j in range(1, dim-1):
            d[j] = BETA_plane[i,j]*T_plane[i - 1,j] + (1.0 - 2.0*BETA_plane[i,j])*T_plane[i,j] + BETA_plane[i,j]*T_plane[i+1,j]

        # now solve the system with the Thomas algorithm
        c2[0] = c[0] / b[0]
        d2[0] = d[0] / b[0]

        for j in range(1, dim-1):
            c2[j] = c[j] / (b[j] - c2[j-1]*a[j])
            d2[j] = (d[j] - d2[j-1]*a[j]) / (b[j] - c2[j-1]*a[j])

        for j in range(dim-2, 0,-1):
            T_plane[i,j] = d2[j] - c2[j] * T_plane[i,j+1] 

    # second part of the calculation (timestep n+1/2 -> n+1)
    # y-sweep
    for j in range(1, dim-1):
        # create the implicit coefficient matrix for the first halfstep
        a = -BETA_plane[:,j]
        c = -BETA_plane[:,j]
        b = 2.0*BETA_plane[:,j] + 1
        
        b[0] = 1.
        b[dim-1] = 1.
        a[0] = 0.
        a[dim-1] = 0.
        c[0] = 0.
        c[dim-1] = 0.

        # calculate the explicit solution vector first halfstep
        d[0] = T_plane[0,j]
        for i in range (1, dim-1):
            d[i] = ALPHA_plane[i,j] * T_plane[i,j-1] + (1.0 - 2.0 * ALPHA_plane[i,j]) * T_plane[i,j] + ALPHA_plane[i,j] * T_plane[i,j+1]

        # now solve the system with the Thomas algorithm
        c2[0] = c[0] / b[0]
        d2[0] = d[0] / b[0]

        for i in range(1, dim-1):
            c2[i] = c[i] / (b[i] - c2[i-1] * a[i])
            d2[i] = (d[i] - d2[i-1] * a[i]) / (b[i] - c2[i-1]*a[i])

        for i in range (dim-2, 0, -1):
            T_plane[i,j] = d2[i] - c2[i]*T_plane[i+1,j]


for timestep in range(0, nb_timesteps):
    
    print (timestep)

    for base_vec in range(0,3):
        for plane in range(0, dim-1):
            ADI(base_vec, plane)
                
#%% main
#if __name__ == "__main__":

fig = go.Figure(
    data=go.Volume(
            x=X.flatten(),
            y=Y.flatten(),
            z=Z.flatten(),
            value=T.flatten(),
            isomin=700,
            isomax=1200,
            opacity=0.1, # needs to be small to see through all surfaces
            surface_count=40, # needs to be a large number for good volume rendering
            # slices_x=dict(show=True, locations=[int(dim/2)]),
            # slices_y=dict(show=True, locations=[int(dim/2)]),
            slices_z=dict(show=True, locations=[int(dim/2)]),
            
        ),
    )
fig.update_layout(
    dict(
        title=go.layout.Title(text="Temperature"),
        ),
    )
fig.show()

#%% end of file
