"""
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
"""

import os		# mainly for system and environment settings
import string	# mainly for the 'split'-function
import sys

python_version = f"{sys.version_info[0]}.{sys.version_info[1]}"
python_include = f"/usr/include/python{python_version}"
python_lib = f"-lpython{python_version}"
boost_lib = f"-lboost_python{sys.version_info[0]}{sys.version_info[1]}"

# constructs a new environment and passes $PATH to scons
e = Environment(ENV = os.environ, CC = 'g++')

# append include-pathes to the environment
e.Append(CPPPATH = ['./include', python_include, './src/newmat10'])

## append libraries and -pathes to the environment
e.Append(LIBS = ['-lgomp', python_lib, boost_lib])
e.Append(LIBPATH = ['/usr/lib'])

## append compile-time flags to the environment
# e.Append(CCFLAGS = '-g -pg -Wall -fopenmp -fPIC -DBOOST_PYTHON_STATIC_MODULE')
#e.Append(CCFLAGS = '-g -Wall -fPIC -DBOOST_PYTHON_STATIC_MODULE')
#e.Append(CCFLAGS = '-O3 -Wall -fopenmp -fPIC -DBOOST_PYTHON_STATIC_MODULE')
e.Append(CCFLAGS = '-O4 -Wall -fopenmp -fPIC -DBOOST_PYTHON_STATIC_MODULE')

## append linker flags to the environment
# e.Append(LINKFLAGS = '-g -pg -DBOOST_PYTHON_STATIC_MODULE')
#e.Append(LINKFLAGS = '-g -DBOOST_PYTHON_STATIC_MODULE')
e.Append(LINKFLAGS = '-O4 -DBOOST_PYTHON_STATIC_MODULE')
#e.Append(LINKFLAGS = '-DBOOST_PYTHON_STATIC_MODULE')

sources = Glob('src/melange/*.cpp')
sources += Glob('src/newmat10/*.cpp')

e.SharedLibrary('melange', sources, SHLIBPREFIX = '', SHLIBSUFFIX='.so')
