# Implementation and Usage of thermal diffusion in Melange

## Implementation
Implemeneted as a 3D ADI-Finite Difference algorithm in the new class `HeatDiff` in `heat_diff.ccp` and `heat_diff.h`.

The only public function is:
```c++
void CalculateHeatDiffusion(Real dt, Real alpha_max);
```

- `dt` is the chosen timestep (in seconds).
- `alpha_max` is the maximum allowed value for alpha, the coeffcicient used to solve the finite element grid. See also under 'material/ADI-method.pdf' for an explanation. This is comparable to the CFL-condition, also stability is not the issue here, but accuracy. Good values for `alpha_max` are _always_ < 1. Generally: the smaller the better. 0.25 seems to be a good first shot.

## Main calculation
is done in the `ADI()`-method, which will then call succesifly the ADI-functions for every XY-, XZ- and YZ-plane in the system.

Relevant functions:
```c++
void ADI();
void ADI_xplane(int xind);
void ADI_yplane(int yind);
void ADI_zplane(int zind);
```

## Messaging
Communication between the particle swarm and the FD grid takes place by mapping the values of the particles to the respective location in the FD grid and vice versa.

Relevant private methods are:
```c++
void MapTemperatureAndConstantsToGrid();
void SetParticleTemperatureFromGrid();
```

## Interpolation
Since the arrangement of particles is not rectangular, there will necessarily be empty spaces in the FD grid. These gaps are filled by a (very simple) k-next-neighbor interpolation method. 

Method:
```c++
void KNN(int i, int j, int k, int required_nb_of_neighbours);
```
searches for Nan-positions in the grid, and searches successifly through the next neighbour-hulls until at least `required_nb_of_neighbours` is found.

The assigned value is then the average of the neighbours.

![Definition of `hulls`, used for the knn-search.](images/knn-scheme.png)

## Scaling

Scaling is donce with regard to the spatial system size, which, in geological systems, can be very large.

Application in 
```
ADI()
```
Cp eg `Langtangen: Scaling of Differential Equations: p85` in the `materials` directory of this repo.