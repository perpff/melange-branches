/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXPERIMENT_SETTINGS_H
#define EXPERIMENT_SETTINGS_H

#include "model.h"

class ExperimentSettings
{
public:
    
    // con- & destructors
    ExperimentSettings(Model *mod);
    ~ExperimentSettings();
    // vars
    Model *model;

	void ActivateGravity();
	
	// multiplies exclusively the strength of second-neighbor springs by a factor
	// useful if we want to further reduce this breaking strength with regard to
	// fisrt-neig-springs. This operation can be helpful if the localization of 
	// 2nd-neig-fracturing is to large.
	void ScaleSecNeigStrength(Real fac);

	// removes springs in a certain volume, making the material granular while the rest of the model remains a lattice
	void RemoveSprings (Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax);

    // distributions
    void SetGaussianStrengthDistribution (Real g_mean, Real g_sigma, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax); // factor is an arbitrary factor to increase the breaking strength AFTER the distribution has been applied
    void SetGaussianYoungandVDistribution ( Real g_mean, Real g_sigma, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax);
    
    // this multiplies the youngs-modulus with a factor of the particles inbetween the given coordinates
    void HardenParticles ( Real factor, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax );

    // these are 'real' values, as observed in nature
    void SetScalingFactor(Real scalefactor);
    void SetDensityForAllParticles(Real density);
    void SetDensity(Real density, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2);
    void SetStandardYoungsModulusAndV(Real real_young, Real v);
    // as usual, x1 < x2, y1 < y2, etc.
	void SetRealYoungAndV(Real real_young, Real v, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2);

    // fixes the second-nearest system-boundaries partially in space, just as is done with nearest-neighbour boundaries
    void DefineAndFixSecondNextNeighbourBoundaries();

    // sets a no-break condition for all the particles below a certain threshold
    // the no_break is set for 1st- and second neighbour particles
    void SetNoBreakY(Real depth);
    // sets a no-break condition for all the particles between the given
    // values on the x-axis
    void SetNoBreakX(Real left, Real right);
    // resets the nobreak-condition of the springs
    void ResetNoBreak();

    // Cuts the springs (1st and 2nd neighbour) which cross a predefined plane on a
    // certain x-position, normal to the x-axis
    void SeparateParticlesCrossingDistinctYZPlane(Real planex, Real plane_upper_y, Real plane_lower_y, Real plane_front_z, Real plane_back_z);
    // this function weakens the breaking strength of springs crossing a plane normal to x by a factor
    void WeakenSpringsCrossingDistinctYZPlane(Real young_factor, Real break_factor, Real planex, Real plane_upper_y, Real plane_lower_y, Real plane_front_z, Real plane_back_z);
	
	// Severes springs crossing an arbitrarily positioned plane.
	// It's defined by the upper- and lower x,y,z-boundary and affects the points therein
	// Plane itself is defined by 3 points in X/Y/Z
    void SeverSpringsCrossingPlane( Real p_1_x, Real p_1_y, Real p_1_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z, Real xlimit1, Real xlimit2, Real ylimit1, Real ylimit2, Real zlimit1, Real zlimit2);
	// Severes springs crossing an arbitrarily positioned triangular area
	void SeverSpringsCrossingTriangle( Real p_1_x, Real p_1_y, Real p_1_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z );
	// Severes springs crossing an arbitrarily positioned circular area. The orientation of the plane is defined via the center point and 2 arbitrary points on the plane. The extend of the circle is defined by the center and the radius.
	void SeverSpringsCrossingCircularPlane( Real radius, Real center_x, Real center_y, Real center_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z );

    // does as it says: the breaking-strength to a certain value in the given cuboidal domain
    // Breaking strength is the "real" breaking strength in Pa, standard is 1 MPa.
    void ChangeBreakingStrength(Real breaking_strength, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax);

    // fixes/unfixes bonds/positions of boundary-particles
    // the name should say everything
    void EnableSideWallBreakingInZ();
    void EnableSideWallBreakingInZAndX();
    void Unfix_z();
    void Unfix_y();
    void Unfix_x();
	void Pin_Back_Boundary();
	void Pin_Front_Boundary();
	void Pin_Bottom_Boundary();
	void Pin_Top_Boundary();
	void Pin_Left_Boundary();
	void Pin_Right_Boundary();
	
	/*************************
	 * viscoelasticity
	 ************************/
	// default is 1e23 (upper crust viscosity)
	void SetViscosity(Real viscosity, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2);
	// default is false
	void ActivateLatticeViscoelasticity();
    // set timestep
    void SetTimeStep(Real timestep);
	
	// this pins an additional number of layers at the bottom of the model
	// nb = 0: only bottom-boundary, nb = 1: bottom-boundar + additional layer, nb = 2: ...
	void Pin_Bottom_Boundary_Several_Layer(int nb);

	// whether the failure of springs is based on the comlplete stress tensor or only on the spring extension
	// default: on the full stress tensor
	void Set_Tensor_Failure_Mode(bool stress_tensor_based_failure);

	// activate the power law viscosity for the particles in the range
    // power law viscosity defaults to olivine (cp Dixon et al. (values below), Wallner, Kirby and Kronenberg)
	void SetPowerLawViscosity( Real minx , Real maxx , Real miny , Real maxy , Real minz , Real maxz, 
		Real A = exp(4.08), Real Ea = 5e5, Real Va = 9e-6, Real n =3.5);

    // temperature of the particle, in Kelvin
    void SetTemperature( Real minx , Real maxx , Real miny , Real maxy , Real minz , Real maxz, Real temperature);

    // angle of internal friction for the mohr-coulomb criterion. Default is pi/6
    void Set_Angle_Of_Internal_Friction(Real angle_of_internal_friction, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax);

    // Activate Mohr Coulomb Failure
    void Activate_Mohr_Coulomb_Failure(bool activate);

    /**************
    Temperature  
    ***************/
    void SetupTestingEnvironmentForThermalDiffusion();

protected:

private:

};

#include <boost/python.hpp>
#include <boost/python/def.hpp>

using namespace boost::python;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(SetPowerLawViscosity_overloads, SetPowerLawViscosity, 6, 10)

#endif // EXPERIMENT_SETTINGS_H
