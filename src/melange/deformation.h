/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEFORMATION_H
#define DEFORMATION_H

#include "model.h"

//newmat (matrix-operations)
#include <newmatap.h>
#include <iostream>
#include "../newmat10/newmatap.h"
#include <iomanip>
#include "../newmat10/newmatio.h"

class Deformation
{
public:
    Deformation(Model *mod);
    ~Deformation();

	// The two functions are the most importamt, most general deformation functions 
	void DeformLattice(Real x = 0.01, Real z = 0.0);
    void DeformLatticeSimpleShear(Real z);

    // The following functions are for special settings only, for instance for wing crack formation
    void DeformLatticeWingCrack(Real y);
    void DeformLatticeXMid(Real x, Real height, Real left, Real right);
    void DeformLatticeSandbox(Real x, Real height, Real left, Real right);
	void DeformLatticeXMid2(Real x, Real height, Real left1, Real right1, Real left2, Real right2);
    void VerticallyIncreasingXStrain(Real maxx, Real min_height = 0.0, Real max_height = 1.0);

protected:
private:
    Model *model;
};

#include <boost/python.hpp>
#include <boost/python/def.hpp>

using namespace boost::python;
using namespace boost::python;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(DeformLattice_overloads, DeformLattice, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(VerticallyIncreasingXStrain_overloads, VerticallyIncreasingXStrain, 1, 3)

#endif // DEFORMATION_H
