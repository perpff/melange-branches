/*
Copyright (C) 2023 Till Sachau <tsa@posteo.de>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "heat_diff.h"

HeatDiff::HeatDiff(Model *mod)
{
    model = mod;

    srand(time(NULL));
}

HeatDiff::~HeatDiff()
{
    // dtor
}

/*
This is the main and only public interface for the heat diffusion model.
*/
void HeatDiff::CalculateHeatDiffusion(Real dt, Real alpha_max)
{

    res = max({model->nb_of_particles_in_x, model->nb_of_particles_in_y, model->nb_of_particles_in_z});

    xlength = model->xlength;
    ylength = model->ylength;
    zlength = model->zlength;

    dx = xlength / res;
    dy = ylength / res;
    dz = zlength / res;

    nb_of_particles = model->nb_of_particles;
    length_scale = model->scale_factor;

    // error control (cfl-like)
    if (alpha_fac > 1.0)
    {
        cout << "In general, alpha_fac should be smaller than 1.0 to maintain a more-or-less acceptable error" << endl;
        cout << "Better would be a value < 0.5, and preferably < 0.25." << endl << endl;
        cout << "Of course we have balance speed vs accuracy. But 1.0 is just to dangerous." << endl << endl;
        cout << "Exiting now." << endl;
        exit(1);
    }

    this->alpha_fac = alpha_max;

    this->dt = dt;
    this->dt_loop = dt;
    this->n_loops = 1;


    // map particle temperatures and relevant constants to the finite difference grid
    MapTemperatureAndConstantsToGrid();

    ADI();

    // export results to the particles
    SetParticleTemperatureFromGrid();
}

/*
Need to map particle temperatures to the finite difference grid first.
If there is no particle in a particular cell, the temperature will be set by a KNN mechanism.
*/
void HeatDiff::MapTemperatureAndConstantsToGrid()
{
    int i, j, k;

    // init important physical properties and temperature
    C_grid.resize(res, vector<vector<Real>>(res, vector<Real>(res, nan(""))));
    D_grid.resize(res, vector<vector<Real>>(res, vector<Real>(res, nan(""))));
    H_grid.resize(res, vector<vector<Real>>(res, vector<Real>(res, nan(""))));
    temperature_grid.resize(res, vector<vector<Real>>(res, vector<Real>(res, nan(""))));

    // buffers
    C_grid_buf.resize(res, vector<vector<Real>>(res, vector<Real>(res, nan(""))));
    D_grid_buf.resize(res, vector<vector<Real>>(res, vector<Real>(res, nan(""))));
    H_grid_buf.resize(res, vector<vector<Real>>(res, vector<Real>(res, nan(""))));
    temperature_grid_buf.resize(res, vector<vector<Real>>(res, vector<Real>(res, nan(""))));


    for (int n = 0; n < nb_of_particles; n++)
    {
        i = int(model->particle_list[n].pos[0] / dx);
        j = int(model->particle_list[n].pos[1] / dy);
        k = int(model->particle_list[n].pos[2] / dz);

        // temperature
        temperature_grid[i][j][k] = model->particle_list[n].temperature;

        // parameters
        C_grid[i][j][k] = model->particle_list[n].thermal_conductivity;
        D_grid[i][j][k] = model->particle_list[n].density;
        H_grid[i][j][k] = model->particle_list[n].heat_capacity;
    }

    C_grid_buf = C_grid;
    D_grid_buf = D_grid;
    H_grid_buf = H_grid;
    temperature_grid_buf = temperature_grid;

    for (i = 0; i < res; i++)
    {
        for (j = 0; j < res; j++)
        {
            for (k = 0; k < res; k++)
            {
                if (isnan(temperature_grid[i][j][k]))
                {
                    KNN(i, j, k, 6);

                    if (isnan(temperature_grid[i][j][k]) )
                    {
                        cout << "temperature_grid[" << i << "][" << j << "][" << k << "] is still NaN" << endl;
                    }
                }
            }
        }
    }

}

/*
The main control function of the actual heat diffusion calculation.
*/
void HeatDiff::ADI()
{
    // we need to calculate some kind of CFL-criterion and loop accordingly
    // CFL needs to be <= 1
    // not sure if CFL is the correct designation for second-order spatial systems

    /***
     * Below we calculate the maximum allowed timestep size
    ***/

    // determine the lowest / highest parameters
    vector<Real> C_max_vec(res*res, 0.0), H_min_vec(res*res, 0.0), D_min_vec(res*res, 0.0);
    Real C_max, H_min, D_min;

    for (int i = 0; i < res; i++)
    {
        for (int j = 0; j < res; j++)
        {
            C_max_vec[i*res + j] = *max_element(C_grid[i][j].begin(), C_grid[i][j].end());
            D_min_vec[i*res + j] = *min_element(D_grid[i][j].begin(), D_grid[i][j].end());
            H_min_vec[i*res + j] = *min_element(H_grid[i][j].begin(), H_grid[i][j].end());
        }
    }

    Real dl_min = min({dx, dy, dz}) * length_scale;
    Real max_length = max({xlength, ylength, zlength}) * length_scale;

    cout << "max_length: " << max_length << endl;

    C_max = *max_element(C_max_vec.begin(), C_max_vec.end());
    D_min = *min_element(D_min_vec.begin(), D_min_vec.end());
    H_min = *min_element(H_min_vec.begin(), H_min_vec.end());

    Real dt_max = (2.0 * D_min * H_min * dl_min * dl_min * alpha_fac * max_length * max_length) / C_max;

    // define the Loop parameters
    if (dt_max < dt)
    {
        n_loops = int (dt / dt_max) + 1;
        dt_loop = dt / Real(n_loops);
    } 
    else
    {
        n_loops = 1;
        dt_loop = dt;
    }

    // set up helper matrices, using the just calculated dt_loop
    Alpha.resize(res, vector<vector<Real>>(res, vector<Real>(res, 0.0)));
    Beta.resize(res, vector<vector<Real>>(res, vector<Real>(res, 0.0)));

    for (int i = 0; i < res; i++)
    {
        for (int j = 0; j < res; j++)
        {
            for (int k = 0; k < res; k++)
            {
                Alpha[i][j][k] = (C_grid[i][j][k] * dt_loop) / (2 * D_grid[i][j][k] * H_grid[i][j][k] * dx * dx * max_length*max_length);
                Beta[i][j][k] = (C_grid[i][j][k] * dt_loop) / (2 * D_grid[i][j][k] * H_grid[i][j][k] * dy * dy * max_length*max_length);
            }
        }
    }

    // loop over all 3 dimensions and calculate the new heat distriubtion plane by plane
    cout << "number of heat diffusion loops (derived from alpha): " << n_loops << endl;
    cout << "counting loops: ";
    
    for (int n = 0; n < n_loops; n++)
    {

        cout << n+1 << " ";

        for (int xind = 0; xind < res; xind++)
        {
            ADI_xplane(xind);
        }

        for (int yind = 0; yind < res; yind++)
        {
            ADI_yplane(yind);
        }

        for (int zind = 0; zind < res; zind++)
        {
            ADI_zplane(zind);
        }
    }
    cout << endl;
}

void HeatDiff::ADI_xplane(int xind)
{
    vector<Real> a(res, 0.0), b(res, 0.0), c(res, 0.0);
    vector<Real> c2(res, 0.0), d(res, 0.0), d2(res, 0.0);

    // first part of the calculation (timestep n -> n+1/2)
    // x-sweep
    for (int i = 1; i < res - 1; i++)
    {
        a[0] = 0.0;
        a[res - 1] = 0.0;
        b[0] = 1.0;
        b[res - 1] = 1.0;
        c[0] = 0.0;
        c[res - 1] = 0.0;

        for (int j = 1; j < res - 1; j++)
        {
            // create the implicit coefficient matrix for the first halfstep
            a[j] = -Alpha[xind][i][j];
            b[j] = 1 + 2 * Alpha[xind][i][j];
            c[j] = -Alpha[xind][i][j];
        }

        // calculate the explicit solution vector for the first halfstep
        d[0] = temperature_grid[xind][i][0];
        for (int j = 1; j < res - 1; j++)
        {
            d[j] = Beta[xind][i][j] * temperature_grid[xind][i - 1][j] + (1.0 - 2.0 * Beta[xind][i][j]) * temperature_grid[xind][i][j] + Beta[xind][i][j] * temperature_grid[xind][i + 1][j];
        }

        // now solve with Thomas algortithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int j = 1; j < res - 1; j++)
        {
            c2[j] = c[j] / (b[j] - c2[j - 1] * a[j]);
            d2[j] = (d[j] - d2[j - 1] * a[j]) / (b[j] - c2[j - 1] * a[j]);
        }

        for (int j = res - 2; j > 0; j--)
        {
            temperature_grid[xind][i][j] = d2[j] - c2[j] * temperature_grid[xind][i][j + 1];
        }
    }

    // second part of the calculation (timestep n+1/2 -> n+1)
    // y-sweep
    for (int j = 1; j < res - 1; j++)
    {
        a[0] = 0.0;
        a[res - 1] = 0.0;
        b[0] = 1.0;
        b[res - 1] = 1.0;
        c[0] = 0.0;
        c[res - 1] = 0.0;

        for (int i = 1; i < res - 1; i++)
        {
            // create the implicit coefficient matrix for the first halfstep
            a[i] = -Beta[xind][i][j];
            b[i] = 1 + 2 * Beta[xind][i][j];
            c[i] = -Beta[xind][i][j];
        }

        // calculate the explicit solution vector for the first halfstep
        d[0] = temperature_grid[xind][0][j];
        for (int i = 1; i < res - 1; i++)
        {
            d[i] = Alpha[xind][i][j] * temperature_grid[xind][i][j - 1] + (1.0 - 2.0 * Alpha[xind][i][j]) * temperature_grid[xind][i][j] + Alpha[xind][i][j] * temperature_grid[xind][i][j + 1];
        }

        // now solve with Thomas algortithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int i = 1; i < res - 1; i++)
        {
            c2[i] = c[i] / (b[i] - c2[i - 1] * a[i]);
            d2[i] = (d[i] - d2[i - 1] * a[i]) / (b[i] - c2[i - 1] * a[i]);
        }

        for (int i = res - 2; i > 0; i--)
        {
            temperature_grid[xind][i][j] = d2[i] - c2[i] * temperature_grid[xind][i + 1][j];
        }
    }
}

void HeatDiff::ADI_yplane(int yind)
{
    vector<Real> a(res, 0.0), b(res, 0.0), c(res, 0.0);
    vector<Real> c2(res, 0.0), d(res, 0.0), d2(res, 0.0);

    // first part of the calculation (timestep n -> n+1/2)
    // x-sweep
    for (int i = 1; i < res - 1; i++)
    {
        a[0] = 0.0;
        a[res - 1] = 0.0;
        b[0] = 1.0;
        b[res - 1] = 1.0;
        c[0] = 0.0;
        c[res - 1] = 0.0;

        for (int j = 1; j < res - 1; j++)
        {
            // create the implicit coefficient matrix for the first halfstep
            a[j] = -Alpha[i][yind][j];
            b[j] = 1 + 2 * Alpha[i][yind][j];
            c[j] = -Alpha[i][yind][j];
        }

        // calculate the explicit solution vector for the first halfstep
        d[0] = temperature_grid[i][yind][0];
        for (int j = 1; j < res - 1; j++)
        {
            d[j] = Beta[i][yind][j] * temperature_grid[i - 1][yind][j] + (1.0 - 2.0 * Beta[i][yind][j]) * temperature_grid[i][yind][j] + Beta[i][yind][j] * temperature_grid[i + 1][yind][j];
        }

        // now solve with Thomas algortithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int j = 1; j < res - 1; j++)
        {
            c2[j] = c[j] / (b[j] - c2[j - 1] * a[j]);
            d2[j] = (d[j] - d2[j - 1] * a[j]) / (b[j] - c2[j - 1] * a[j]);
        }

        for (int j = res - 2; j > 0; j--)
        {
            temperature_grid[i][yind][j] = d2[j] - c2[j] * temperature_grid[i][yind][j + 1];
        }
    }

    // second part of the calculation (timestep n+1/2 -> n+1)
    // y-sweep
    for (int j = 1; j < res - 1; j++)
    {
        a[0] = 0.0;
        a[res - 1] = 0.0;
        b[0] = 1.0;
        b[res - 1] = 1.0;
        c[0] = 0.0;
        c[res - 1] = 0.0;

        for (int i = 1; i < res - 1; i++)
        {
            // create the implicit coefficient matrix for the first halfstep
            a[i] = -Beta[i][yind][j];
            b[i] = 1 + 2 * Beta[i][yind][j];
            c[i] = -Beta[i][yind][j];
        }

        // calculate the explicit solution vector for the first halfstep
        d[0] = temperature_grid[0][yind][j];
        for (int i = 1; i < res - 1; i++)
        {
            d[i] = Alpha[i][yind][j] * temperature_grid[i][yind][j - 1] + (1.0 - 2.0 * Alpha[i][yind][j]) * temperature_grid[i][yind][j] + Alpha[i][yind][j] * temperature_grid[i][yind][j + 1];
        }

        // now solve with Thomas algortithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int i = 1; i < res - 1; i++)
        {
            c2[i] = c[i] / (b[i] - c2[i - 1] * a[i]);
            d2[i] = (d[i] - d2[i - 1] * a[i]) / (b[i] - c2[i - 1] * a[i]);
        }

        for (int i = res - 2; i > 0; i--)
        {
            temperature_grid[i][yind][j] = d2[i] - c2[i] * temperature_grid[i + 1][yind][j];
        }
    }
}

void HeatDiff::ADI_zplane(int zind)
{
    vector<Real> a(res, 0.0), b(res, 0.0), c(res, 0.0);
    vector<Real> c2(res, 0.0), d(res, 0.0), d2(res, 0.0);

    // first part of the calculation (timestep n -> n+1/2)
    // x-sweep
    for (int i = 1; i < res - 1; i++)
    {
        a[0] = 0.0;
        a[res - 1] = 0.0;
        b[0] = 1.0;
        b[res - 1] = 1.0;
        c[0] = 0.0;
        c[res - 1] = 0.0;

        for (int j = 1; j < res - 1; j++)
        {
            // create the implicit coefficient matrix for the first halfstep
            a[j] = -Alpha[i][j][zind];
            b[j] = 1 + 2 * Alpha[i][j][zind];
            c[j] = -Alpha[i][j][zind];
        }

        // calculate the explicit solution vector for the first halfstep
        d[0] = temperature_grid[i][0][zind];
        for (int j = 1; j < res - 1; j++)
        {
            d[j] = Beta[i][j][zind] * temperature_grid[i - 1][j][zind] + (1.0 - 2.0 * Beta[i][j][zind]) * temperature_grid[i][j][zind] + Beta[i][j][zind] * temperature_grid[i + 1][j][zind];
        }

        // now solve with Thomas algortithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int j = 1; j < res - 1; j++)
        {
            c2[j] = c[j] / (b[j] - c2[j - 1] * a[j]);
            d2[j] = (d[j] - d2[j - 1] * a[j]) / (b[j] - c2[j - 1] * a[j]);
        }

        for (int j = res - 2; j > 0; j--)
        {
            temperature_grid[i][j][zind] = d2[j] - c2[j] * temperature_grid[i][j + 1][zind];
        }
    }

    // second part of the calculation (timestep n+1/2 -> n+1)
    // y-sweep
    for (int j = 1; j < res - 1; j++)
    {
        a[0] = 0.0;
        a[res - 1] = 0.0;
        b[0] = 1.0;
        b[res - 1] = 1.0;
        c[0] = 0.0;
        c[res - 1] = 0.0;

        for (int i = 1; i < res - 1; i++)
        {
            // create the implicit coefficient matrix for the first halfstep
            a[i] = -Beta[i][j][zind];
            b[i] = 1 + 2 * Beta[i][j][zind];
            c[i] = -Beta[i][j][zind];
        }

        // calculate the explicit solution vector for the first halfstep
        d[0] = temperature_grid[0][j][zind];
        for (int i = 1; i < res - 1; i++)
        {
            d[i] = Alpha[i][j][zind] * temperature_grid[i][j - 1][zind] + (1.0 - 2.0 * Alpha[i][j][zind]) * temperature_grid[i][j][zind] + Alpha[i][j][zind] * temperature_grid[i][j + 1][zind];
        }

        // now solve with Thomas algortithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int i = 1; i < res - 1; i++)
        {
            c2[i] = c[i] / (b[i] - c2[i - 1] * a[i]);
            d2[i] = (d[i] - d2[i - 1] * a[i]) / (b[i] - c2[i - 1] * a[i]);
        }

        for (int i = res - 2; i > 0; i--)
        {
            temperature_grid[i][j][zind] = d2[i] - c2[i] * temperature_grid[i + 1][j][zind];
        }
    }
}

/*
 * if a value is missing in the grid, this function will
 * find neighbouring gridpoints and interpolate the value
 * by calculating the average of the neighbouring values
 */
void HeatDiff::KNN(int i, int j, int k, int required_nb_of_neighbours)
{
    int nb_neighbours = 0;

    // temperature
    Real Tval = 0.0;

    // parameters
    Real Cval = 0.0;
    Real Dval = 0.0;
    Real Hval = 0.0;

    int step = 1;

    while (nb_neighbours < required_nb_of_neighbours)
    {
        // fix i
        for (int x : {i - step, i + step})
        {
            if (x < res && !(x < 0))
            {
                for (int y = j - step; y < j + step + 1; y++)
                {
                    if (y < res && !(y < 0))
                    {
                        for (int z = k - step; z < k + step + 1; z++)
                        {
                            if (z < res && !(z < 0))
                            {
                                if (!isnan(temperature_grid_buf[x][y][z]))
                                {
                                    nb_neighbours++;

                                    Tval += temperature_grid_buf[x][y][z];

                                    Cval += C_grid_buf[x][y][z];
                                    Dval += D_grid_buf[x][y][z];
                                    Hval += H_grid_buf[x][y][z];
                                }
                            }
                        }
                    }
                }
            }
        }

        // fix j
        for (int y : {j - step, j + step})
        {
            if (y < res && !(y < 0))
            {
                for (int x = i - step; x < i + step + 1; x++)
                {
                    if (x < res && !(x < 0))
                    {
                        for (int z = k - step; z < k + step + 1; z++)
                        {
                            if (z < res && !(z < 0))
                            {
                                if (!isnan(temperature_grid_buf[x][y][z]))
                                {
                                    if (x < res && y < res && z < res)
                                    {
                                        nb_neighbours++;

                                        Tval += temperature_grid_buf[x][y][z];

                                        Cval += C_grid_buf[x][y][z];
                                        Dval += D_grid_buf[x][y][z];
                                        Hval += H_grid_buf[x][y][z];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // fix k
        for (int z : {k - step, k + step})
        {
            if (z < res && !(z < 0))
            {
                for (int y = j - step; y < j + step; y++)
                {
                    if (y < res && !(y < 0))
                    {
                        for (int x = i - step; x < i + step; x++)
                        {
                            if (x < res && !(x < 0))
                            {
                                if (!isnan(temperature_grid_buf[x][y][z]))
                                {
                                    nb_neighbours++;

                                    Tval += temperature_grid_buf[x][y][z];

                                    Cval += C_grid_buf[x][y][z];
                                    Dval += D_grid_buf[x][y][z];
                                    Hval += H_grid_buf[x][y][z];
                                }
                            }
                        }
                    }
                }
            }
        }

        step++;
    }

    // set the values in the grid variables
    C_grid[i][j][k] = Cval / nb_neighbours;
    D_grid[i][j][k] = Dval / nb_neighbours;
    H_grid[i][j][k] = Hval / nb_neighbours;

    temperature_grid[i][j][k] = Tval / nb_neighbours;
}

/*
After the calculation: map finite difference results to particle temperatures.
*/
void HeatDiff::SetParticleTemperatureFromGrid()
{
    int i, j, k;

    for (int n = 0; n < nb_of_particles; n++)
    {
        i = int(model->particle_list[n].pos[0] / dx);
        j = int(model->particle_list[n].pos[1] / dy);
        k = int(model->particle_list[n].pos[2] / dz);

        model->particle_list[n].temperature = temperature_grid[i][j][k];
    }
}
