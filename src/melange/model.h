/*
   Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

   This file is part of Melange.

   Melange is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   Melange is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with Melange.  If not, see <http://www.gnu.org/licenses/>.
   */

#ifndef MODEL_H
#define MODEL_H

#include "particle.h"
#include "global_defs"

#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <sys/stat.h>

//newmat (matrix-operations)
#include "../newmat10/newmatap.h"
#include <iomanip>
#include "../newmat10/newmatio.h"

//boost.python
#include <boost/python.hpp>
#include <boost/python/def.hpp>

using namespace std;

class Model
{

    public:

        Model();
        ~Model();

        void Info();

        /* 
           the following properties will be exposed read/write to python
           */

        /* 
           the following properties will be exposed read-only to python
           */
        // dimensionsions of lists
        int nb_of_particles, nb_of_particles_in_x, nb_of_particles_in_y, nb_of_particles_in_z;

        /* 
           the following functions will be exposed to python
           */
        // returns the particles and puts them into a python list

        // returns the particle with the given index
        // this is the only way to access the particles from python
        // exposing the particle_list directly to python creates a copy of the list
        Particle& GetParticle(int i)
        {
            return particle_list[i];
        }

        // initialize the new model
        void CreateNewModel(int resolution_in_x = 50, Real xlength = 1.0, Real ylength = 1.0, Real zlength = 1.0, 
                bool is_second_neighbour_model = true, bool repulsion = true,  
                bool walls_in_x = false, bool walls_in_z = false, 
                bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);
        void ActivateGravity();
        void MakeRepBox(bool model_definition_mode = false);

        // use periodic boundaries?
        bool use_periodic_boundaries_in_x;
        bool use_periodic_boundaries_in_z;

        // Mohr-Coulomb (shear) failure activated? Default = yes
        void Activate_Mohr_Coulomb_Failure(bool activate);

        // lists of particles
        // rep_box is defined by the repbox_dim_x/y/z and the rep_box_inc_x/y/z (and the system-size)
        Particle *particle_list;

        // repbox and defining variables
        // every repbox_inc_x/y/z must be slightly larger than the particle-diameter,
        // but have to cover the system without gap
        // all variables are defined in the MakeRepBox-function, which has to be called after every deformation
        Particle ****rep_box;
        int repbox_dim_x, repbox_dim_y, repbox_dim_z;		// dimenson
        Real repbox_inc_x, repbox_inc_y, repbox_inc_z;	// the width of a rep_box-cell

        Real particle_radius;
        Real second_neig_radius;

        // dimensions of box
        Real xlength, ylength, zlength;
        Real xlength_initial, ylength_initial, zlength_initial;
        Real wrapping_constant_x;
        Real wrapping_constant_z;

        // timestep
        Real timestep;	// default is 0

        // standard real youngs modulus (translates as 1.0 to the model youngs modulus)
        Real standard_real_young;
        Real standard_v;

        // viscoelastic?
        bool viscoelastic;	// default is false

        void DumpVTKFile(string stdpath);

        // called from other modules, if a particle might have been moved
        void ChangeBox(Particle *tmp_prtcl);

        // attributes on display
        attribute attribute_on_display;
        Real max_range, min_range, abs_max_range, abs_min_range;

        bool repulsion;									// use repulsion? is set to true by standard
        bool is_second_neighbour_model;					// are u a second-neighbour model or just repulsion?
        bool walls_in_x;
        bool walls_in_z;

        // particles on display
        particle_type particletype;

        /**************** scaling ******************/
        // dimensions of the model (in m)
        Real scale_factor; 

        /**************** gravity ******************/
        bool use_gravity_force;

        Real Average_mean_stress();

        /************* fracture mode ***************/
        bool stress_tensor_based_failure; // default: spring failure is based on the full stress tensor
        bool mohr_coulomb_failure;

        void myFunction(int x)
        {
            // Do something with int
        }

        void myFunction(double x, double y)
        {
            // Do something with two doubles
        }

    protected:

    private:
        void MakeHexagonalLattice(Real xsize, Real ysize, Real zsize);
        void DefineParticleNeighbours();
        void AppendSamePlaneNeighbours(Particle **list, Particle *curpar, int listsize);
        void AppendTopPlaneNeighbours(Particle **list, Particle *curpar, int listsize);
        void AppendBottomPlaneNeighbours(Particle **list, Particle *curpar, int listsize);
        void AppendSecondNeighboursToList(Particle **list, Particle *curpar, int listsize);

        void AssignParticleNeighbourPositions();

        // sets backpointer to neighbour-particles
        void SetBackPtr();

        // this function does currently not define a repbox which wraps the way the connected
        // boundary-particles do if the function is called.
        // therefore, the no-break conditions must still be set on the boundaries.
        void MakePeriodicBoundariesOnXAxis();
        void MakePeriodicBoundariesOnZAxis();

        Real GetDistBetweenParticles(Particle* curpar, Particle *neig);
        Real GetAngleFromPositiveXAxis(Particle* curpar, Particle *neig);

        void MarkNextAndSecondNextBoundaryParticles();
        // "Top", "Bottom" are in Y, "Left", "Right" in X, "Front", "Back" in Z
        void MarkTopBottomLeftRightFrontBackNextNeighbourBoundaryParticles();
        void MarkTopBottomLeftRightFrontBackSecondNextNeighbourBoundaryParticles();
        // fix the boundary particles, so they cannot move into certain directions
        void FixBoundaryParticles();

        void VTK_SavePoints(ofstream *store);
        void VTK_SaveBoundaries(ofstream *store);
        void VTK_SaveFractures(ofstream *store);
        void VTK_SavePointData_ParticleNumber(ofstream *store);
        void VTK_SavePointData_Fix_X(ofstream *store);
        void VTK_SavePointData_Fix_Y(ofstream *store);
        void VTK_SavePointData_Fix_Z(ofstream *store);

        void VTK_SavePointData_Temperature(ofstream *store);

        void VTK_SavePointData_Radius(ofstream *store);
        void VTK_SavePointData_YoungsModulus(ofstream *store);

        void VTK_SavePointData_MeanStress(ofstream *store);
        void VTK_SavePointData_DiffStress(ofstream *store);
        void VTK_SavePointData_XStress(ofstream *store);
        void VTK_SavePointData_YStress(ofstream *store);
        void VTK_SavePointData_ZStress(ofstream *store);
        void VTK_SavePointData_XYStress(ofstream *store);
        void VTK_SavePointData_XZStress(ofstream *store);
        void VTK_SavePointData_YZStress(ofstream *store);

        void VTK_SavePointData_Sigma1(ofstream *store);
        void VTK_SavePointData_Sigma2(ofstream *store);
        void VTK_SavePointData_Sigma3(ofstream *store);
        void VTK_SaveVecData_Sigma1_Vec(ofstream *store);
        void VTK_SaveVecData_Sigma2_Vec(ofstream *store);
        void VTK_SaveVecData_Sigma3_Vec(ofstream *store);

        void VTK_SavePointData_X_Pos(ofstream *store);
        void VTK_SavePointData_Y_Pos(ofstream *store);
        void VTK_SavePointData_Z_Pos(ofstream *store);
        void VTK_SavePointData_X_Layer(ofstream *store);
        void VTK_SavePointData_Y_Layer(ofstream *store);
        void VTK_SavePointData_Z_Layer(ofstream *store);

        void VTK_SavePointData_Viscosity(ofstream *store);
        void VTK_SavePointData_Density(ofstream *store);

        void PrepareNewLattice(Real x_size, Real y_size, Real z_size, int x_particles, bool use_periodic_boundaries_in_x, bool use_periodic_boundaries_in_z );
        void CalcSizeAndNbOfParticlesAndSystemDimensions(Real xsize, Real ysize, Real zsize, int x_particles);
        void SetParticleAttribute(attribute att, bool init);
        void SetParticleType(particle_type type);

        void SetHullIntersections();

};

#include <boost/python.hpp>
#include <boost/python/def.hpp>

using namespace boost::python;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(CreateNewModel_overloads, CreateNewModel, 0, 10)

#endif // MODEL_H
