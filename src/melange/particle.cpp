/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "particle.h"

Particle::Particle()
{
	
	radius = 0.0;
	
	for (int i=0; i<12; i++)
	{
		neig_back_ptr[i] = -1;
	}
	for (int i=0; i<6; i++)
	{
		sec_neig_back_ptr[i] = -1;
	}
	
    // viscoelasticity settings
	visc_unconnected_neigh_list.clear();
	
	second_neig_radius = 0.0;

    xx = yy = zz = 0.0;
	young = 1.0;
    
    mbreak = 0.0;
    volume = 0.0;

    for (int i=0; i<6; i++)
    {
        second_neighbour_list[i] = NULL;
        second_neigh_break_Str[i] = 1e6;
        second_neigh_no_break[i] = false;
    }

    for (int i=0; i<12; i++)
    {
        neighbour_list[i] = NULL;
        break_Str[i] = 1e6;
        no_break[i] = false;
    }
    
    br_strength = 1.0;
    
    is_broken = false;
    second_neighbour_is_broken = false;

    // initialized as false, is set in the Model class
    is_boundary = false;
    is_top_boundary = is_bottom_boundary = is_left_boundary = false;
    is_right_boundary = is_front_boundary = is_back_boundary = false;
    
    fix_x = fix_y = fix_z = false;
	
	// default: elastic
	viscoelastic = false;
	
    // domain boundaries (is more neutral than "grain-boundaries" and therefore replaces the term)
    is_domain_boundary = false;

    // initialization of natural material parameters
    density = 2750.0;	// upper crust density
    real_young = standard_real_young  = 53e9;	// granite: 53 GPa

    // for the repulsion-box
    next_in_box = NULL;
	done_for_multithreads = new bool[omp_get_max_threads()];
    for (int i=0; i<omp_get_max_threads(); i++) done_for_multithreads[i] = false;
    done = false;

    // init stress values
    sxx = syy = szz = sxy = sxz = syz = 0.0;
	sigma1 = sigma2 = sigma3 = 0.0;
    sigma1_vec[0] = sigma2_vec[0] = sigma3_vec[0] = 0.0;
    sigma1_vec[1] = sigma2_vec[1] = sigma3_vec[1] = 0.0;
    sigma1_vec[2] = sigma2_vec[2] = sigma3_vec[2] = 0.0;
    mean_stress = diff_stress = 0.0;
    
	wrapping_constant_x = wrapping_constant_z = 1.0;
	xlength = ylength = zlength = 1.0;
	
    walls_in_x = walls_in_z = use_periodic_boundaries_in_x = use_periodic_boundaries_in_z = false;
    is_second_neighbour_model = repulsion = true;
    
    scale_factor = 100000.0;
	use_gravity_force = false;
	
	viscosity = 1e23;

    temperature = 100.0;
	
	// the viscous transformation Matrix. Is initialized as the identity matrix
	T.ReSize(3,3);
	
	T.element(0,0) = 1.0;
	T.element(1,1) = 1.0;
	T.element(2,2) = 1.0;
	T.element(0,1) = 0.0;
	T.element(0,2) = 0.0;
	T.element(1,0) = 0.0;
	T.element(1,2) = 0.0;
	T.element(2,0) = 0.0;
	T.element(2,1) = 0.0;
	
	fg = 0.0;
    fover = 0.0;
    relaxthreshold = 1e-9;
    dynamic_relax_thresh = false;
    
    k1 = k2 = ks1 = ks2 = 1.0;
    
    // poisson
    v = 0.2;

    // failure criterion: default is tensor based
    stress_tensor_based_failure = false;

    // defaults to newtonian viscosity
    power_law_viscosity = false;
    // power law viscosity defaults to olivine (cp Dixon et al. (values below), Wallner, Kirby and Kronenberg)
    A = exp(4.08);
    Ea = 5e5;
    Va = 9e-6;
    n = 3.5;

    thermal_conductivity = 3.0;   // # W/(m*K)
    heat_capacity = 300.0;        // # J/(kg*K)

    // for the mohr-coulomb criterion
    angle_of_internal_friction = pi / 6.0;
    
    // symmetric matrices in newmat use the lower triangle
    e11 = 1.0;
    e21 = 0.0;
    e22 = 1.0;
    e31 = 0.0;
    e32 = 0.0;
    e33 = 1.0;

}

Particle::~Particle()
{
}

Real Particle::GetRepAlen(Particle *neig)
{
    Real k, dx, dy, dz, abs;
    Real alen;

    // all necessary parameters of the ellipsoid matrix have been set in the Relaxation::ViscousFlow-fct!

    if (viscoelastic)
    {

        dx = GetXDist(neig);
        dy = (neig->pos[1] - pos[1]);
        dz = GetZDist(neig);
        abs = sqrt (dx*dx + dy*dy + dz*dz);
        dx /= abs;
        dy /= abs;
        dz /= abs;
        
        k = sqrt(1.0/(e11*dx*dx + 2.0*e21*dx*dy + 2.0*e31*dx*dz + e22*dy*dy + 2.0*e32*dy*dz + e33*dz*dz));
        
        // new particle radius 
        // doesn't need the factor 2 because of averaging below!
        alen = k * radius;

        // particles are symmetric, therefore we can use the same vector again (and don't have to revert it)
        k = sqrt(1.0/(neig->e11*dx*dx + 2.0*neig->e21*dx*dy + 2.0*neig->e31*dx*dz + neig->e22*dy*dy + 2.0*neig->e32*dy*dz + neig->e33*dz*dz));

        // also here: doesn't need the factor 2 because of averaging below!
        return((alen + k*radius));
    }
    else
        return (2.0*radius);
}

void Particle::SetGeometry(Real rad, Real second_rad)
{
    radius = rad;
    second_neig_radius = second_rad;
            
    volume = (4.0/3.0) * pi * radius*radius*radius;
}

void Particle::SetYoungsModulusAndV(Real real_young, Real v)
{
	Particle::real_young = real_young;
	
	young = real_young / standard_real_young;
	Particle::v = v;

	if (is_second_neighbour_model)
	{
		// this case is completely isotropic
		k1 = -(sqrt(8)*radius*young)/(10*v-5);
		k2 = k1 / 4;
		ks1 = ((sqrt(128)*radius*v - sqrt(8)*radius) * young) / (10*v*v + 5*v - 5);
		ks2 = ks1 / 4;
	}
	else if (!is_second_neighbour_model && repulsion)
	{
		// anisotropic. this is E_x, E_y taken from WANG. E_z is different.
		k1 = young * radius * 28.0 / (15.0 * sqrt(2.0));
	}
	else
	{
		cout << endl << "***************" << endl;
		cout << "you need to select a model type" << endl;
		cout << "***************+" << endl;
		
		exit(0);
	}
}

void Particle::BreakBond(Particle* neighbour_particle)
{

    int i;  // counter
    int j = 0;  // Reference for neighbour
    bool first_neighbour_particle = false;
    bool not_a_neighbour = true;

    // find out, whether its a first or a second neighbour
    for (i = 0; i < 12; i++)
    {
        if (neighbour_list[i])
        {
            if (neighbour_list[i] == neighbour_particle)
            {
                first_neighbour_particle = true;
                not_a_neighbour = false;
                neigh = i;

                break;
            }
        }
    }

    if (!first_neighbour_particle)
    {
        for (i = 0; i < 6; i++)
        {
            if (second_neighbour_list[i])
            {
                if (second_neighbour_list[i] == neighbour_particle)
                {
                    not_a_neighbour = false;
                    neigh = i + 12;
                    break;
                }
            }
        }
    }

    if (not_a_neighbour)
    {
        cout << endl << "***************" << endl;
        cout << "this particle is not a neighbour" << endl;
        cout << "Check your code!" << endl;
        cout << "***************+" << endl;

        exit(0);
    }

    //-----------------------------------------------------------------
    // first break the neighbour
    // find neighbour and break
    //-----------------------------------------------------------------

    if (first_neighbour_particle)                                                             // so its a first neighbour
    {
        for (i = 0; i < 12; i++) {
            if (neighbour_particle->neighbour_list[i])
            {
                if (neighbour_particle->neighbour_list[i] == this)
                {
                    j = i;                                                      // thats the neighbour
                    break;
                }
            }
        }

        neighbour_particle->neighbour_list[j] = 0;                           // neighbour gone
        neighbour_particle->is_broken = true;                                // particle has a broken bond
        neighbour_particle->repulsion = true; 								// consider particle in the repulsion-calculation
        neighbour_particle->break_Str[j] = 0.0;                              // breakingstrength is zero
        neighbour_particle->is_domain_boundary = true;                       // particle is now part of grain boundary

        //-----------------------------------------------------
        // now the particle itself
        //-----------------------------------------------------
        neighbour_list[neigh] = 0;
        is_broken = true;
        repulsion = true; 														// consider particle in the repulsion-calculation
        break_Str[neigh] = 0.0;
    }
    else                                                                        // if its a second neighbour
    {
        for (i = 0; i < 6; i++)
        {
            if (neighbour_particle->second_neighbour_list[i])
            {
                if (neighbour_particle->second_neighbour_list[i] == this)
                {
                    j = i;                                                      // thats the neighbour
                    break;
                }
            }
        }

        neighbour_particle->second_neighbour_list[j] = 0;          // neighbour gone
        neighbour_particle->second_neighbour_is_broken = true;    	// particle has a broken bond
       	neighbour_particle->repulsion = true; 						// consider particle in the repulsion-calculation
        neighbour_particle->second_neigh_break_Str[j] = 0.0;       // breakingstrength is zero
        neighbour_particle->is_domain_boundary = true;             // particle is now part of grain boundary

        //-----------------------------------------------------
        // now the particle itself
        //-----------------------------------------------------
        second_neighbour_list[neigh-12] = 0;
        second_neighbour_is_broken = true;
        repulsion = true; 														// consider particle in the repulsion-calculation
        second_neigh_break_Str[neigh-12] = 0.0;
    }

    is_domain_boundary = true;
    mbreak = 0.0;

}

void Particle::BreakBond(int neighbour_number)
{

    this->neigh = neighbour_number;

    int i;  // counter
    int j = 0;  // Reference for neighbour

    //-----------------------------------------------------------------
    // first break the neighbour
    // find neighbour and break
    //-----------------------------------------------------------------

    if (neigh < 12)                                                             // so its a first neighbour
    {
        for (i = 0; i < 12; i++) {
            if (neighbour_list[neigh]->neighbour_list[i])
            {
                if (neighbour_list[neigh]->neighbour_list[i] == this)
                {
                    j = i;                                                      // thats the neighbour
                }
            }
        }

        neighbour_list[neigh]->neighbour_list[j] = 0;                           // neighbour gone
        neighbour_list[neigh]->is_broken = true;                                // particle has a broken bond
        neighbour_list[neigh]->repulsion = true; 								// consider particle in the repulsion-calculation
        neighbour_list[neigh]->break_Str[j] = 0.0;                              // breakingstrength is zero
        neighbour_list[neigh]->is_domain_boundary = true;                       // particle is now part of grain boundary

        //-----------------------------------------------------
        // now the particle itself
        //-----------------------------------------------------
        neighbour_list[neigh] = 0;
        is_broken = true;
        repulsion = true; 														// consider particle in the repulsion-calculation
        break_Str[neigh] = 0.0;
    }
    else                                                                        // if its a second neighbour
    {
        for (i = 0; i < 6; i++)
        {
            if (second_neighbour_list[neigh-12]->second_neighbour_list[i])
            {
                if (second_neighbour_list[neigh-12]->second_neighbour_list[i] == this)
                {
                    j = i;                                                      // thats the neighbour
                }
            }
        }

        second_neighbour_list[neigh-12]->second_neighbour_list[j] = 0;          // neighbour gone
        second_neighbour_list[neigh-12]->second_neighbour_is_broken = true;    	// particle has a broken bond
       	second_neighbour_list[neigh-12]->repulsion = true; 						// consider particle in the repulsion-calculation
        second_neighbour_list[neigh-12]->second_neigh_break_Str[j] = 0.0;       // breakingstrength is zero
        second_neighbour_list[neigh-12]->is_domain_boundary = true;             // particle is now part of grain boundary

        //-----------------------------------------------------
        // now the particle itself
        //-----------------------------------------------------
        second_neighbour_list[neigh-12] = 0;
        second_neighbour_is_broken = true;
        repulsion = true; 														// consider particle in the repulsion-calculation
        second_neigh_break_Str[neigh-12] = 0.0;
    }

    is_domain_boundary = true;
    mbreak = 0.0;

}

void Particle::BreakBond()
{
    int i;  // counter
    int j = 0;  // Reference for neighbour

    //-----------------------------------------------------------------
    // first break the neighbour
    // find neighbour and break
    //-----------------------------------------------------------------

    if (neigh < 12)                                                             // so its a first neighbour
    {
        for (i = 0; i < 12; i++) {
            if (neighbour_list[neigh]->neighbour_list[i])
            {
                if (neighbour_list[neigh]->neighbour_list[i] == this)
                {
                    j = i;                                                      // thats the neighbour
                }
            }
        }

        neighbour_list[neigh]->neighbour_list[j] = 0;                           // neighbour gone
        neighbour_list[neigh]->is_broken = true;                                // particle has a broken bond
        neighbour_list[neigh]->repulsion = true; 								// consider particle in the repulsion-calculation
        neighbour_list[neigh]->break_Str[j] = 0.0;                              // breakingstrength is zero
        neighbour_list[neigh]->is_domain_boundary = true;                       // particle is now part of grain boundary

        //-----------------------------------------------------
        // now the particle itself
        //-----------------------------------------------------
        neighbour_list[neigh] = 0;
        is_broken = true;
        repulsion = true; 														// consider particle in the repulsion-calculation
        break_Str[neigh] = 0.0;
    }
    else                                                                        // if its a second neighbour
    {
        for (i = 0; i < 6; i++)
        {
            if (second_neighbour_list[neigh-12]->second_neighbour_list[i])
            {
                if (second_neighbour_list[neigh-12]->second_neighbour_list[i] == this)
                {
                    j = i;                                                      // thats the neighbour
                }
            }
        }

        second_neighbour_list[neigh-12]->second_neighbour_list[j] = 0;          // neighbour gone
        second_neighbour_list[neigh-12]->second_neighbour_is_broken = true;    	// particle has a broken bond
       	second_neighbour_list[neigh-12]->repulsion = true; 						// consider particle in the repulsion-calculation
        second_neighbour_list[neigh-12]->second_neigh_break_Str[j] = 0.0;       // breakingstrength is zero
        second_neighbour_list[neigh-12]->is_domain_boundary = true;             // particle is now part of grain boundary

        //-----------------------------------------------------
        // now the particle itself
        //-----------------------------------------------------
        second_neighbour_list[neigh-12] = 0;
        second_neighbour_is_broken = true;
        repulsion = true; 														// consider particle in the repulsion-calculation
        second_neigh_break_Str[neigh-12] = 0.0;
    }

    is_domain_boundary = true;
    mbreak = 0.0;

}

void Particle::WeakenBond(Real young_factor, Real break_factor)
{
    int i;  // counter
    int j = 0;  // Reference for neighbour

    //-----------------------------------------------------------------
    // first break the neighbour
    // find neighbour and break
    //-----------------------------------------------------------------

    if (neigh < 12)                                                             // so its a first neighbour
    {
        for (i = 0; i < 12; i++) {
            if (neighbour_list[neigh]->neighbour_list[i])
            {
                if (neighbour_list[neigh]->neighbour_list[i] == this)
                {
                    j = i;                                                      // thats the neighbour
                }
            }
        }

        neighbour_list[neigh]->break_Str[j] *= break_factor;

        //-----------------------------------------------------
        // now the particle itself
        //-----------------------------------------------------
        break_Str[neigh] = 0.0;
    }
    else                                                                        // if its a second neighbour
    {
        for (i = 0; i < 6; i++)
        {
            if (second_neighbour_list[neigh-12]->second_neighbour_list[i])
            {
                if (second_neighbour_list[neigh-12]->second_neighbour_list[i] == this)
                {
                    j = i;                                                      // thats the neighbour
                }
            }
        }

        second_neighbour_list[neigh-12]->second_neigh_break_Str[j] *= break_factor;       // breakingstrength is zero

        //-----------------------------------------------------
        // now the particle itself
        //-----------------------------------------------------
        second_neigh_break_Str[neigh-12] = 0.0;
    }

}

// takes care of periodic boudnaries, if they are activated
Real Particle::GetXDist( Particle *curpar )
{
    Real x = curpar->pos[0] - pos[0];

    // in case of peridic boundaries we have to take the (left and right) borders into account
    if ( use_periodic_boundaries_in_x )
    {
        if ( ( curpar->is_left_boundary && is_right_boundary ) )
        {
                x = x + wrapping_constant_x;
        }
        else if (curpar->is_right_boundary && is_left_boundary)
        {
                x = x - wrapping_constant_x;
        }
    }

    return(x);
}

Real Particle::GetZDist( Particle *curpar )
{
    Real z = curpar->pos[2] - pos[2];

    // in case of peridic boundaries we have to take the (left and right) borders into account
    if ( use_periodic_boundaries_in_z )
    {
        if ( ( curpar->is_back_boundary && is_front_boundary ) )
        {
            z = z + wrapping_constant_z;
        }
        else if (curpar->is_front_boundary && is_back_boundary)
        {
            z = z - wrapping_constant_z;
        }
    }

    return(z);
}

void Particle::InitModelFlags(bool repulsion, bool is_second_neighbour_model, bool walls_in_x, bool walls_in_z)
{
	/*
	 * There is no need to consider repulsion for an individual particle
	 * if no bonds are broken and if the system itself 
	 * is not set up as a pure repulsion model
	 */
	 
	Particle::is_second_neighbour_model = is_second_neighbour_model;
	
	Particle::walls_in_x = walls_in_x;
	Particle::walls_in_z = walls_in_z;
			
	if (is_second_neighbour_model)
	{
		Particle::repulsion = false;
	}
	else
	{
		Particle::repulsion = repulsion;
	}
}

