/*
Copyright (C) 2023 Till Sachau <tsa@posteo.de>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEAT_DIFF_H
#define HEAT_DIFF_H

#include "model.h"
#include "global_defs"

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm> // provides the 'min' and 'max' function

#include <omp.h>

using namespace std;

class HeatDiff
{
public:

    HeatDiff(Model *mod);
    ~HeatDiff();

    void CalculateHeatDiffusion(Real dt, Real alpha_max);

protected:

private:

    // variables
    Model *model;

    int res;
    int nb_of_particles;

    Real xlength, ylength, zlength;
    Real dx, dy, dz;

    // scaling
    Real lc, tvc, tc;

    Real length_scale;

    Real dt;
    Real dt_loop;
    int n_loops;

    // alpha_fac controls the accuracy and stability of the ADI method
    // very much similar to cfl_fac in hydrodynamics
    
    // generally: 0 < alpha_fac < 1.0 (the smaller, the more accurate)
    
    // in 3D, it is likely that a good value is 0.25
    Real alpha_fac;
    
    vector<vector<vector<Real>>> temperature_grid;
    vector<vector<vector<Real>>> C_grid; // thermal conductivity
    vector<vector<vector<Real>>> D_grid; // density
    vector<vector<vector<Real>>> H_grid; // heat capacity
    
    // these are buffers, to be used in the interpolation
    vector<vector<vector<Real>>> temperature_grid_buf;
    vector<vector<vector<Real>>> C_grid_buf;            // thermal conductivity
    vector<vector<vector<Real>>> D_grid_buf;            // density
    vector<vector<vector<Real>>> H_grid_buf;            // heat capacity

    // helper matrices
    vector<vector<vector<Real>>> Alpha; 
    vector<vector<vector<Real>>> Beta;

    // methods
    void MapTemperatureAndConstantsToGrid();
    void SetParticleTemperatureFromGrid();
    
    void ADI();
    
    void ADI_xplane(int xind);
    void ADI_yplane(int yind);
    void ADI_zplane(int zind);

    // interpolation with the (very primitive) KNN method, in case there are NaNs in the grid
    void KNN(int i, int j, int k, int required_nb_of_neighbours);

};

#endif // HEAT_DIFF_H