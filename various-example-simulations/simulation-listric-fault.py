#!/usr/bin/python 

"""
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, time
from math import *
from melange import *

workingdir = os.getcwd()

"""
create necessary objects
"""
m = Model();
e = ExperimentSettings(m)
d = Deformation(m)
r = Relaxation(m)

"""
initialize
"""
## CreateNewModel(int resolution_in_x = 50, float xlength = 1.0, float ylength = 1.0, float zlength = 1.0,
m.CreateNewModel( 60, 1.0, 0.5, 0.1, 
True, True, 		# bool is_second_neighbour_model = true, bool repulsion = true, 
False, False, 		# bool walls_in_x = false, bool walls_in_z = false, 
True, True )		# bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);

r.SetRelaxationThreshold(5e-9)

# in the relaxation-function 
r.SetOverRelaxationFactor(1.0)

"""
Elasticity
"""
## this resets a new (real) standard-youngs-modulus for every particle as given and
## resets the youngs modulus on model-level to 1
## also resets for the springk's 
## The DEFAULT youngs modulus is 53e9 (!) 
e.SetStandardYoungsModulusAndV(80e9, 0.20)
# e.SetRealYoungAndV(120e9, 0.20, 0.425, 0.575, 0.4, 0.5, -10.0, 10.0)

# multiplies the Youngs modulus of springs with random value (with mean +/- standard-deviation)
#e.SetGaussianKandYoungDistribution(1.0, 0.1, -10.0, 10.0, -10.0, 10.0, -10.0, 10.0)

"""
General (real) physical parameters
"""
## DEFAULT for the density is 2700 kg/m**3
e.SetDensityForAllParticles(2700)               # upper crust density
## DEFAULT for the systemsize is 100 km
e.SetScalingFactor(100000)                      # 100 km

"""
viscoelasticity
"""
e.SetViscosity( 1e25, 0.0, 1.0, 0.45, 0.5, 0.0, 1.0 )
e.SetViscosity( 1e24, 0.0, 1.0, 0.4, 0.45, 0.0, 1.0 )
e.SetViscosity( 1e23, 0.0, 1.0, 0.3, 0.4, 0.0, 1.0 )
e.SetViscosity( 5e22, 0.0, 1.0, 0.2, 0.3, 0.0, 1.0 )
e.SetViscosity( 1e22, 0.0, 1.0, 0.1, 0.2, 0.0, 1.0 )
e.SetViscosity( 5e21, 0.0, 1.0, 0.0, 0.1, 0.0, 1.0 )
# e.SetViscosity( 6e21, 0.0, 1.0, 0.1, 0.15, 0.0, 1.0 )
# e.SetViscosity( 3e21, 0.0, 1.0, 0.05, 0.1, 0.0, 1.0 )
# e.SetViscosity( 1e21, 0.0, 1.0, 0.0, 0.05, 0.0, 1.0 )



"""
Control over the boundaries,
whether they are fixed, are 
allowed to break, etc.
"""
# e.EnableSideWallBreakingInZ()
e.EnableSideWallBreakingInZAndX()
# e.Unfix_z()
# e.Unfix_y()
e.Unfix_x()

"""
breaking-strength of springs
"""
# reduce breaking strength for ALL particles to a usable value, adapt domains later
#e.ChangeBreakingStrength( 0.0125, -10.0, 10.0, -10.0, 10.0, -10.0, 10.0 )

# EXCEPT if you set a Gaussian Distribution for the strength: this will set the value to +/- the standard deviation
#e.SetGaussianStrengthDistribution(1.0, 0.1, 0, 10, 0, 10, 0, 10)

'''
2nd adapt breaking strength of domains
'''
#e.ChangeBreakingStrength ( 0.01, -10.0, 10.0, -10.0, 10.0, -10.0, 10.0 )
# e.ChangeBreakingStrength ( -2e8, -10.0, 10.0, 0.45, 0.5, -10.0, 10.0 )
e.ChangeBreakingStrength ( -3e8, -10.0, 10.0, 0.0, 0.5, -10.0, 10.0 )
# e.ChangeBreakingStrength ( -4e8, -10.0, 10.0, 0.2, 0.3, -10.0, 10.0 )
# e.ChangeBreakingStrength ( -5e8, -10.0, 10.0, 0.0, 0.2, -10.0, 10.0 )
# e.ChangeBreakingStrength ( -12e8, 0.425, 0.575, 0.4, 0.5, -10.0, 10.0 )
# e.ChangeBreakingStrength ( -1e8, 0.4, 0.6, 0.47, 0.5, -10.0, 10.0 )
# e.ChangeBreakingStrength ( -1e7, -10.0, 10.0, 0.4, 0.65, -10.0, 10.0 )

e.SetGaussianStrengthDistribution(1.0, 0.3, 0, 10, 0, 10, 0, 10)

"""
sets a block of harder particles where the  youngs modulus
is multiplied by a factor
block is defined by xmin, xmax, ymin, ...
void HardenParticles ( float factor, float xmin, float xmax, float ymin, float ymax, float zmin, float zmax );
"""
# e.HardenParticles( 2.0, 0.45, 0.55, 0.0, 1.0, 0.45, 0.55 )
# e.ChangeBreakingStrength( 2.0, 0.25, 0.45, 0.3, 0.5, 0.4, 0.6 )

e.ActivateGravity()
# use viscoelasticity (i.e.: change the young's modulus for most particles accordingly)
e.ActivateLatticeViscoelasticity()

# default failure mode is false = tensor-less
e.SetTensorFailureMode(False)

# e.SetTemperature( float minx , float maxx , float miny , float maxy , float minz , float maxz, float temperature);
# e.SetPowerLawViscosity(float minx , float maxx , float miny , float maxy , float minz , float maxz, float A, float Ea, float Va, float n)
# e.SetPowerLawViscosity(0.0, 1.0, 0.0, 1.0, 0.0, 1.0)

# void ScaleSecNeigStrength(float fac);
# e.ScaleSecNeigStrength(sqrt(2.0))
# e.ScaleSecNeigStrength(0.25)
# e.ScaleSecNeigStrength(0.707106781)

e.Activate_Mohr_Coulomb_Failure(True)
e.Set_Angle_Of_Internal_Friction(pi/3.73, 0.0, 1.0, 0.0, 0.3, 0.0, 1.0)
e.Set_Angle_Of_Internal_Friction(pi/4.11, 0.0, 1.0, 0.3, 0.4, 0.0, 1.0)
e.Set_Angle_Of_Internal_Friction(pi/5.2, 0.0, 1.0, 0.4, 0.5, 0.0, 1.0)

# output and summary in cli
m.Info()
r.Info()

e.SetNoBreakY (1.0)
#e.SetNoBreakX (0.0, 0.15)
#e.SetNoBreakX (0.85, 1.0)

### output as vtk-files
m.DumpVTKFile("test-res75-original")
# seconds; timestep is set to 0 bei default
e.SetTimeStep(157680000000000)				# 5000 a == 10 m spreading
for t in range(1,10,1):
	r.Relax()
	m.DumpVTKFile("test_"+`t`)
m.DumpVTKFile("test-res75-0")

e.SetTimeStep(15768000000)
e.ResetNoBreak()
e.SetNoBreakY (0.05)

"""
deformation, relaxation, output
"""

for t in range ( 1, 10001, 1 ):

	# deformation

	# void DeformLattice(float x = 0.01, float z = 0.0)
	d.DeformLattice (0.0001) # == 1000m
	
	# d.VerticallyIncreasingXStrain(0.0001, 0.2, 0.4)
	# d.DeformLatticeXMid (0.001, 0.35, 0.35, 0.65)
	# d.DeformLatticeXMid (0.00001, 0.4, 0.4, 0.6)
	# d.DeformLatticeSandbox(0.0001, 0.35, 0.35, 0.65)

	# void DeformLatticeXMidVolConst(float x, float z, float neck_height, float left, float right)
	# d.DeformLatticeXMidVolConst(0.01, 0.0, 0.3, 0.4, 0.6)

	# we need to measure and compare the performance of the relaxation
	# therefore: save the start.time
	start = time.time()

	# relaxation
	r.Relax()

	# now: print the time-difference
	print("Relaxation took:", time.time() - start)

	# save results
	# if t < 50:
	# m.DumpVTKFile("test-res75-elastic-large-steps"+`t`)
	if t % 25 == 0:
		m.DumpVTKFile("test-res75-elastic-small-steps_new_visc_"+`t`)

print "end"
