#!/usr/bin/python 

"""
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, time
from melange import *

workingdir = os.getcwd()

"""
create necessary objects
"""
m = Model();
e = ExperimentSettings(m)
d = Deformation(m)
r = Relaxation(m)

"""
initialize
"""
m.CreateNewModel ( 
21, 				# int resolution_in_x = 50,
0.5, 1.0, 0.5, 		# double xlength = 1.0, double ylength = 1.0, double zlength = 1.0,
True, True,  		# bool is_second_neighbour_model = true, # bool repulsion = true,
False, False, 		# bool walls_in_x = false, bool walls_in_z = false, 
True, True )		# bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);

r.SetRelaxationThreshold(5e-9)
r.SetOverRelaxationFactor(1.0)
# this unsets any Relaxation-factors and replaces it with a threshold which depends on the particle displacement, default = false
r.ActivateDynamicORF(False)

e.SetStandardYoungsModulusAndV(80e9, 0.2)

'''
General (real) physical parameters
'''
## DEFAULT for the density is 2700 kg/m**3
e.SetDensityForAllParticles(2700)               # upper crust density
## DEFAULT for the systemsize is 100 km
e.SetScalingFactor(10000)                      # 20 km

e.ActivateGravity()

### output and summary in cli
m.Info()
r.Info()

e.SetNoBreakY (1.0)
#e.Unfix_z()
#e.Unfix_x()

start = time.time()
r.Relax()

print("Initial relaxation took:", time.time() - start)

m.DumpVTKFile("res-"+`0`)

print "end"

