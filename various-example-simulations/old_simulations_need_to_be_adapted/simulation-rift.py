#!/usr/bin/python 

import os, time
from melange import *

workingdir = os.getcwd()

"""
create necessary objects
"""
m = Model();
e = ExperimentSettings(m)
d = Deformation(m)
r = Relaxation(m)

"""
initialize
"""
m.CreateNewModel( 
100, 				# int resolution_in_x = 50,
1.0, 0.5, 0.1,	 		# double xlength = 1.0, double ylength = 1.0, double zlength = 1.0,
True,  				# bool is_second_next_neighbour_model = true, 
True,				# bool repulsion = true, 
False, False, 			# bool walls_in_x = false, bool walls_in_z = false, 
True, True )			# bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);

r.SetRelaxationThreshold(5e-9)
## good for granular materials
#r.SetOverRelaxationFactor(2.044)
## good for lattices:
r.SetOverRelaxationFactor(1.0)
# this unsets any Relaxation-factors and replaces it with a threshold which depends on the particle displacement, default = false
r.ActivateDynamicORF(False)

e.SetStandardYoungsModulusAndV(80e9, 0.20);

e.SetRealYoungAndV(160e9, 0.20, 0.45, 0.55, 0.35, 0.5, 0.0, 1.0)
e.SetRealYoungAndV(160e9, 0.20, 0.0, 1.0, 0.0, 0.35, 0.0, 1.0)

'''
General (real) physical parameters
'''
## DEFAULT for the density is 2700 kg/m**3
e.SetDensityForAllParticles(2700)               # upper crust density
e.SetDensity(3200, 0.0, 1.0, 0.0, 0.35, 0.0, 1.0)
e.SetDensity(3200, 0.45, 0.55, 0.35, 0.4, 0.0, 1.0)
## DEFAULT for the systemsize is 100 km
e.SetScalingFactor(200000)                      	# 170 mm

'''
viscoelasticity
'''
# sets the viscosity for a box of particles. box given bei x1/x2, y1/y2, z1/z2
# void SetViscosity(double viscosity, double x1, double x2, double y1, double y2, double z1, double z2);
# default for the viscosity is 1e23, the value for the upper crust
e.SetViscosity( 1e24, 0.0, 1.0, 0.45, 0.5, 0.0, 1.0 )
e.SetViscosity( 1e22, 0.0, 1.0, 0.0, 0.35, 0.0, 1.0 )
e.SetViscosity( 1e22, 0.45, 0.55, 0.35, 0.4, 0.0, 1.0 )
#~ e.SetViscosity( 5e24, 0.0, 1.0, 0.6, 0.7, 0.0, 1.0 )
#~ e.SetViscosity( 1e24, 0.0, 1.0, 0.5, 0.6, 0.0, 1.0 )
#~ e.SetViscosity( 5e23, 0.0, 1.0, 0.35, 0.5, 0.0, 1.0 )
#~ e.SetViscosity( 1e14, 0.25, 0.75, 0.0, 0.025, 0.0, 1.0 )

# seconds; timestep is set to 0 bei default
e.SetTimeStep(125000 * 365 * 24 * 60 * 60)	# 125000 a


#e.MoveModelCoordinates(0.1, 0.1)

'''
the breaking strength set in "e.ChangeBreakingStrength" 
is the "real" breaking strength in Pa. Standard is 1 MPa.

"SetGaussianStrengthDistribution" multiplies the given breaking strength by a factor
'''

#e.RemoveSprings(-10.0, 10.0, 0.025, 10.0, -10.0, 10.0)
#e.ChangeBreakingStrength( -1.3e8, -10.0, 10.0, -10.0, 10.0, -10.0, 10.0 )
e.ChangeBreakingStrength( -7e7, -10.0, 10.0, -10.0, 10.0, -10.0, 10.0 )

e.SetGaussianStrengthDistribution(1.0, 0.01, 0, 1.0, 0, 1.0, 0, 1.0)

#e.Pin_Bottom_Boundary_Several_Layer(1)
e.Pin_Bottom_Boundary()
e.ActivateGravity()
## use viscoelasticity (i.e.: change the young's modulus for most particles accordingly)

"""
Control over the boundaries,
whether they are fixed, are 
allowed to break, etc.
"""
#e.EnableSideWallBreakingInZ()
#e.EnableSideWallBreakingInZAndX()
e.Unfix_z()
e.Unfix_x()
#e.Unfix_y()

e.ActivateLatticeViscoelasticity()

### output and summary in cli
m.Info()
r.Info()

e.SetNoBreakY (1.0)
for t in range(1,5,1):
	start = time.time()
	r.Relax()
print("initial relaxation, step", t, " of 10. Relaxation took:", time.time() - start)
e.ResetNoBreak()

# e.SeverSpringsCrossingPlane( 0.45, 0.0, 0.5, 0.45, 1.0, 0.5, 0.45, 0.0, 1.0, 0.1, 0.9, 0.4, 1.0, 0.0, 1.0 )
# e.SeverSpringsCrossingPlane( 0.55, 0.0, 0.5, 0.55, 1.0, 0.5, 0.55, 0.0, 1.0, 0.1, 0.9, 0.4, 1.0, 0.0, 1.0 )

e.SetNoBreakY (0.3)

start = time.time()
r.Relax()
print("Initial relaxation took:", time.time() - start)
m.DumpVTKFile("res-"+`0`)

r.Relax()

for t in range (1, 1000, 1):

	#d.DeformLattice(0.00125)
	d.DeformLatticeXMid2(0.00125, 0.7, 0.4, 0.46, 0.54, 0.6)

	start = time.time()

	r.Relax()
	
	print("Timestep:", t, ". Relaxation took:", time.time() - start)
	
	#if t % 50 == 0:
	m.DumpVTKFile("res-"+`t`)

print "end"
